package edu.dnu.fpm.pz.calc.interfaces;

/**
 * This is calc interface
 */
public interface Calc {
    double addition(double firstArg, double secondArg);

    double substraction(double firstArg, double secondArg);

    double multiplication(double firstArg, double secondArg);

    double division(double firstArg, double secondArg);
}
