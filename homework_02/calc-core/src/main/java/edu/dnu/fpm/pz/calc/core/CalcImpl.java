package edu.dnu.fpm.pz.calc.core;


import edu.dnu.fpm.pz.calc.interfaces.Calc;

/**
 * This is implementation of Calc
 */
public class CalcImpl implements Calc {
    public double addition(double firstArg, double secondArg) {
        return firstArg + secondArg;
    }

    public double substraction(double firstArg, double secondArg) {
        return firstArg - secondArg;
    }

    public double multiplication(double firstArg, double secondArg) {
        return firstArg * secondArg;
    }

    public double division(double firstArg, double secondArg) {
        double tmp = 0;
        try {
            tmp = firstArg / secondArg;
        } catch (ArithmeticException e) {
            System.out.println("Division by zero");
            tmp = 0;
        }
        return tmp;
    }
}
