package edu.dnu.fpm.pz;

import org.junit.*;
import org.junit.rules.ExpectedException;
import org.mockito.*;

import static org.junit.Assert.*;

public class ProcessorTest {
    @Rule
    public ExpectedException thrown = ExpectedException.none();
    @Mock
    Producer producer;
    @Mock
    Consumer consumer;
    @Captor
    private ArgumentCaptor<String> valueCaptor;
    @InjectMocks
    Processor processor = new Processor();
    @Before
    public void setup() { MockitoAnnotations.initMocks(this);}
    @Test
    public void testProcessWithException(){
        //GIVEN
        thrown.expect(IllegalStateException.class);
        Mockito.when(producer.produce()).thenReturn(null);
        //WHEN
        processor.process();
        //THEN
    }
    @Test
    public void testProcessWithoutException(){
        //GIVEN
        String producedString = "Magic value";
        Mockito.when(producer.produce()).thenReturn(producedString);
        //WHEN
        processor.process();
        //THEN
        Mockito.verify(consumer).consume(valueCaptor.capture());
        String actual = valueCaptor.getValue();
        assertEquals(producedString, actual);
    }
}
