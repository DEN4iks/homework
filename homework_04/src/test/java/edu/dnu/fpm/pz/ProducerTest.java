package edu.dnu.fpm.pz;

import org.junit.Assert;
import org.junit.Test;

public class ProducerTest {

    @Test
    public void testProduce() {
        //GIVEN
        Producer producer = new Producer();
        String expectedString = "Magic value";

        //WHEN
        String actual = producer.produce();

        //THEN
        Assert.assertNotNull(actual);
        Assert.assertEquals(expectedString, actual);
    }
}
