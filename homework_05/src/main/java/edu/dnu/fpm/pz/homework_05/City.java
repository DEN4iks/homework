package edu.dnu.fpm.pz.homework_05;

public class City {
    private String name;
    private String state;
    private long population;

    public City(String name, String state, long population)
    {
        this.name = name;
        this.state = state;
        this.population = population;
    }

    public long getPopulation() {
        return population;
    }

    public String getName() {
        return name;
    }

    public String getState() {
        return state;
    }

    public static int compare(City c1, City c2)
    {
        return c1.getPopulation() >= c2.getPopulation() ? 1 : -1;
    }
}

