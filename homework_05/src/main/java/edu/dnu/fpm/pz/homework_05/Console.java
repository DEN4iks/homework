package edu.dnu.fpm.pz.homework_05;

import java.util.*;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;

public class Console {
    public static void main( String[] args )
    {

        System.out.println("===============Stream API (#1 task of 4)===============");
        ArrayList<Integer> numbers = new ArrayList<Integer>();
        numbers.add(3);
        numbers.add(44);
        numbers.add(4);
        numbers.add(5);
        System.out.println("List of Integer");
        for (Integer s : numbers) {
            System.out.print(s + " ");
        }
        System.out.println();
        System.out.println("Convert every element to String on every iteration (with adding 'e' or 'o')");
        Streams.convertFromNumberToString(numbers);
        System.out.println();


        System.out.println("===============Stream API (#2 task of 4)===============");
        ArrayList<City> cities = new ArrayList<City>();
        cities.addAll(Arrays.asList());

        try {
            Map<String, City> max = Streams.getLargestCityPerState(cities);
            System.out.println("Largest Cities Per States");
            for (String key : max.keySet()) {
                System.out.println("State: " + max.get(key).getState() + ", Largest city: " +  max.get(key).getName());
            }

            System.out.println();
        } catch (NoSuchElementException ex) {
            System.out.println(ex);
        }




        System.out.println("===============Stream API (#3 task of 4)===============");
        Stream<Integer> odd = IntStream.iterate(1, item -> item + 2).limit(10).boxed();
        Stream<Integer> even = IntStream.iterate(2, item -> item + 2).limit(7).boxed();
        List<Integer> actual = Streams.zip(odd, even).collect(toList());
        System.out.println("Result List:");
        for(Integer s: actual)
        {
            System.out.print(s + " ");
        }
        System.out.println();
    }
}
