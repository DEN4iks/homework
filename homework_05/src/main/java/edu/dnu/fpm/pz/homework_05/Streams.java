package edu.dnu.fpm.pz.homework_05;

import java.util.*;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Streams {
    // Write a method that alternates elements from the streams first and second, stopping when one of them runs out of elements.
    public static <T> Stream<T> zip(Stream<T> first, Stream<T> second) {
        Queue<T> elementsA = first.collect(Collectors.toCollection(LinkedList::new));
        Queue<T> elementsB = second.collect(Collectors.toCollection(LinkedList::new));
        return Stream.generate(new Supplier<T>() {
            boolean first = true;

            @Override
            public T get() {
                Queue<T> queue = first ? elementsA : elementsB;
                first = !first;
                return queue.poll();
            }
        }).limit(Math.min(elementsA.size(), elementsB.size()) * 2);
    }

    // Using Stream API write a method that returns a comma separated string based on a given list of integers.
    // Each element should be preceded by the letter 'e' if the number is even, and preceded by the letter 'o' if the number is odd.
    public static String convertFromNumberToString(List<Integer> numbers)
    {
        Stream<Integer> stream = numbers.stream();
        String toString =
        stream
                .map(x -> x%2 == 0 ? "e" + String.valueOf(x) : "o" + String.valueOf(x))
                .collect(Collectors.joining(", "));

        System.out.println(toString);
        return toString;
    }

    // Using Stream API implement a method that produces the largest city per state
    public static Map<String, City> getLargestCityPerState(List<City> list) throws NoSuchElementException{
        Stream<City> stream = list.stream();
        Map<String, List<City>> states = stream.collect(Collectors.groupingBy(City::getState));

        return states.entrySet().stream()
                .collect(Collectors.toMap(
                        Map.Entry::getKey,
                        e -> e.getValue().stream()
                                .max(Comparator.comparing(City::getPopulation))
                                .orElseThrow(NoSuchElementException::new)
                ));
    }
}


