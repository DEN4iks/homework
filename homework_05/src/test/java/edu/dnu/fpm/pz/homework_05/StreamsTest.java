package edu.dnu.fpm.pz.homework_05;

import static java.util.stream.Collectors.*;

import java.util.*;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import org.junit.Assert;
import org.junit.Test;

public class StreamsTest {
    @Test
    public void test1() {
        // GIVEN
        Stream<Integer> odd = IntStream.iterate(1, item -> item + 2).limit(7).boxed();
        Stream<Integer> even = IntStream.iterate(2, item -> item + 2).limit(10).boxed();
        List<Integer> actual = Streams.zip(odd, even).collect(toList());
        List<Integer> expected = IntStream.rangeClosed(1, 14).boxed().collect(toList());

        // WHEN
        Optional<Integer> oddReduce = IntStream.iterate(1, item -> item + 2).limit(7).boxed().reduce((x, y)->x+y);
        Optional<Integer> evenReduce = IntStream.iterate(2, item -> item + 2).limit(10).boxed().reduce((x, y)->x+y);

        // THEN
        Assert.assertEquals(expected, actual);
        Assert.assertFalse( oddReduce.get() > evenReduce.get());
        Assert.assertTrue(evenReduce.get() > oddReduce.get());
        Assert.assertTrue(evenReduce.get() != oddReduce.get());
    }

    @Test
    public void test2() {
        //GIVEN
        ArrayList<City> cities = new ArrayList<City>();

        String expectedNameFirst = "Dnipro";
        String expectedNameSecond = "Alushta";

        cities.addAll(Arrays.asList(new City[]{
                new City("Dnipro", "first", 123),
                new City("Kiev", "first", 100),
                new City("Kharkiv", "second", 125),
                new City("Alushta", "second", 127),
        }));

        //WHEN
        Map<String, City> max = Streams.getLargestCityPerState(cities);


        // THEN
        Assert.assertEquals(expectedNameFirst, max.get("first").getName());
        Assert.assertEquals(expectedNameSecond, max.get("second").getName());
    }

    @Test
    public void test3() {
        // GIVEN
        ArrayList<Integer> numbers = new ArrayList<Integer>();
        String expected = "o1, e2, o3, e4";

        // WHEN
        numbers.add(1);
        numbers.add(2);
        numbers.add(3);
        numbers.add(4);

        // THEN
        System.out.println(expected);
        String actual = Streams.convertFromNumberToString(numbers);
        Assert.assertEquals(expected, actual);
    }
}
