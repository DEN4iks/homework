package edu.dnu.fpm.pz.list.interfaces;

import edu.dnu.fpm.pz.myexception.MyException;

public interface List<T>{
    void showAll();

    T get(int index) throws MyException;

    int getSize();

    boolean isEmpty();

    void add(int index,T elem) throws MyException;

    T remove(int index) throws MyException;  //throws Exception;

    String toString();

    void unshift(T elem);

    void push (T elem);

    T shift() throws MyException;

    T pop () throws MyException;

    T getFirstElem();

    T getLastElem();

}


