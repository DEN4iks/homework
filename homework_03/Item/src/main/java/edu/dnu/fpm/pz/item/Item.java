package edu.dnu.fpm.pz.item;

public class Item {
    private int a;
    public Item(int a)
    {
        this.a = a;
    }

    @Override public String toString()
    {
        return String.valueOf(a);
    }

}
