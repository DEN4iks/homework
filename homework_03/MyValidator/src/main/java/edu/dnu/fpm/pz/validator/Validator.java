package edu.dnu.fpm.pz.validator;

import edu.dnu.fpm.pz.myexception.MyException;

public class Validator {
    static public void checkIndex(int index, int size) throws MyException
    {
        if (index < 0) {
            throw new MyException("index < 0, failed index: " + index);
        }
        else if(index >= size) { 
            throw new MyException("index >= size, failed index: " + index);
        }
    }
}

