package edu.dnu.fpm.pz.LinkedList;
import static org.junit.Assert.assertEquals;

import edu.dnu.fpm.pz.item.Item;
import edu.dnu.fpm.pz.myexception.MyException;
import org.junit.*;
import edu.dnu.fpm.pz.MyLinkedList.MyLinkedList;
import org.junit.rules.ExpectedException;

public class TestMyLinkedList {
    @Rule
    public ExpectedException thrown = ExpectedException.none();

    private MyLinkedList<Item> myLinkedList;
    private Item myData = new Item(5);

    @Before
    public  void PrepareData(){
        myLinkedList = new MyLinkedList<>();
        myLinkedList.push(myData);
    }

    @After
    public  void AfterTest(){
        myLinkedList = null;
    }

    @Test
    public void testDefaultConstructor(){
        int expectedsize = 0;
        myLinkedList = new MyLinkedList<>();
        int result = myLinkedList.getSize();
        Assert.assertNotNull(myLinkedList);
        Assert.assertEquals(expectedsize, result);
    }


    @Test
    public void testAddNegativeWithExpectedException() throws MyException {
        //GIVEN
        Item newData = new Item(40);
        int negativeIndex = -4;
        thrown.expect(MyException.class);
        thrown.expectMessage("index < 0");
        //WHEN
        myLinkedList.add(negativeIndex, newData);
        thrown = ExpectedException.none();
        //THEN
    }


    @Test
    public void testAddBigIndexWithExpectedException() throws MyException {
        //GIVEN
        Item newData = new Item(40);
        int bigIndex = 100;
        thrown.expect(MyException.class);
        thrown.expectMessage("index >= size");
        //WHEN
        myLinkedList.add(bigIndex, newData);
        thrown = ExpectedException.none();
        //THEN
    }

    @Test
    public void testAddGoodIndex() throws MyException
    {
        //GIVEN
        Item newData = new Item(34);
        int index = 0;
        //WHEN
        myLinkedList.add(index, newData);
        //THEN
        Assert.assertNotNull(myLinkedList);
        assertEquals(myLinkedList.getSize(), 2);
        assertEquals(myLinkedList.get(0), newData);
    }

    @Test
    public void testAddElementToBegin() throws MyException
    {
        //GIVEN
        Item myData1 = new Item(34);
        Item myData2 = new Item(5);
        Item myData3 = new Item(6);
        //WHEN
        myLinkedList.unshift(myData1);
        myLinkedList.unshift(myData2);
        myLinkedList.unshift(myData3);
        //THEN
        Assert.assertNotNull(myLinkedList);
        assertEquals(myLinkedList.getSize(), 4);
        assertEquals(myLinkedList.get(0), myData3);
    }

    @Test
    public void testAddElementToEnd() throws MyException
    {
        //GIVEN
        Item myData1 = new Item(34);
        Item myData2 = new Item(5);
        Item myData3 = new Item(6);
        //WHEN
        myLinkedList.push(myData1);
        myLinkedList.push(myData2);
        myLinkedList.push(myData3);
        //THEN
        Assert.assertNotNull(myLinkedList);
        assertEquals(myLinkedList.getSize(), 4);
        assertEquals(myLinkedList.get(myLinkedList.getSize() - 1), myData3);
    }

    @Test
    public void testGetFirstElem() throws  MyException
    {
        //GIVEN
        Item newData;
        //WHEN
        newData = myLinkedList.get(0);
        //THEN
        assertEquals(newData, myLinkedList.getFirstElem());
    }

    @Test
    public void testGetLastElem() throws MyException
    {
        //GIVEN
        Item newData;
        //WHEN
        newData = myLinkedList.get(myLinkedList.getSize() - 1);
        //THEN
        assertEquals(newData, myLinkedList.getLastElem());
    }


    @Test
    public void testGetNegativeIndexWithExpectedException() throws MyException {
        //GIVEN
        int negativeIndex = -4;
        thrown.expect(MyException.class);
        thrown.expectMessage("index < 0");
        //WHEN
        myLinkedList.get(negativeIndex);
        thrown = ExpectedException.none();
        //THEN
    }


    @Test
    public void testGetBigIndexWithExpectedException() throws MyException {
        //GIVEN
        int bigIndex = 100;
        thrown.expect(MyException.class);
        thrown.expectMessage("index >= size");
        //WHEN
        myLinkedList.get(bigIndex);
        thrown = ExpectedException.none();
        //THEN
    }

    @Test
    public void testGetElem() throws MyException
    {
        //GIVEN
        int index = 0;
        Item newData;
        //WHEN
        newData = myLinkedList.get(index);
        //THEN
        Assert.assertNotNull(myLinkedList);
        assertEquals(myLinkedList.getSize(), 1);
        assertEquals(newData, myData);
    }

    @Test
    public void testRemoveFromBegin() throws MyException
    {
        //GIVEN
        Item myData1 = new Item(4);
        //WHEN
        myLinkedList.unshift(myData1);
        myLinkedList.shift();
        //THEN
        Assert.assertNotNull(myLinkedList);
        assertEquals(myLinkedList.getSize(), 1);
        assertEquals(myData, myLinkedList.getFirstElem());
    }

    @Test
    public void testRemoveFromEnd() throws MyException
    {
        //GIVEN
        Item myData1 = new Item(4);
        //WHEN
        myLinkedList.push(myData1);
        myLinkedList.pop();
        //THEN
        Assert.assertNotNull(myLinkedList);
        assertEquals(myLinkedList.getSize(), 1);
        assertEquals(myData, myLinkedList.getLastElem());
    }


    @Test
    public void testRemoveNegativeIndexWithExpectedException() throws MyException {
        //GIVEN
        int negativeIndex = -4;
        thrown.expect(MyException.class);
        thrown.expectMessage("index < 0");
        //WHEN
        myLinkedList.remove(negativeIndex);
        thrown = ExpectedException.none();
        //THEN
    }


    @Test
    public void testRemoveBigIndexWithExpectedException() throws MyException {
        //GIVEN
        int bigIndex = 100;
        thrown.expect(MyException.class);
        thrown.expectMessage("index >= size");
        //WHEN
        myLinkedList.remove(bigIndex);
        thrown = ExpectedException.none();
        //THEN
    }

    @Test
    public void testRemoveElem() throws MyException
    {
        //GIVEN
        int index = 1;
        Item myData1 = new Item(3);
        Item myData2 = new Item(5);
        //WHEN
        myLinkedList.push(myData1);
        myLinkedList.push(myData2);
        myLinkedList.remove(index);
        //THEN
        Assert.assertNotNull(myLinkedList);
        assertEquals(myLinkedList.getSize(), 2);
        assertEquals(myLinkedList.get(index), myData2);
    }
}
