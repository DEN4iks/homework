package edu.dnu.fpm.pz.MyLinkedList;

import edu.dnu.fpm.pz.list.interfaces.List;
import edu.dnu.fpm.pz.myexception.MyException;
import edu.dnu.fpm.pz.validator.Validator;

public class MyLinkedList<T> implements List<T> {

    private static class Node<T> {
        T element;
        Node<T> next;
        Node<T> previous;

        public Node(T element) {
            this.element = element;
            this.next = null;
            this.previous = null;
        }
    }
    private Node<T> first;
    private Node<T> last;
    public int size = 0;


    public MyLinkedList(int _size, T elem )
    {
        if(_size == 0) {
            makeEmptyList();
        }
        else{
            for(int i = 0; i < _size; i++){
                push(elem);
            }
        }
    }

    public MyLinkedList(){ makeEmptyList();};

    private void makeEmptyList()
    {
        first = last = null;
    }

    @Override public boolean isEmpty() {
        return first == null ? true : false;
    }

    @Override public void showAll() {

        Node<T> tmp = first;
        for (int i = 0; i < size; i++){
            if(tmp == null) {
                break;
            }
            System.out.println(tmp.element.toString());
            tmp = tmp.next;

        }
    }


    @Override public void unshift(T element) {
        Node newNode = new Node(element);
        if (isEmpty()) {
            first = last = newNode;
        } else {
            first.previous = newNode;
            newNode.next = first;
            first = newNode;
        }
        size++;
    }


    @Override public void push(T element) {
        Node newNode = new Node(element);
        if (isEmpty()) {
            first = last = newNode;
        } else {
            last.next = newNode;
            newNode.previous = last;
            last = newNode;
        }
        size++;
    }

    @Override public void add(int index, T element) throws MyException {
        Validator.checkIndex(index, size);
        Node newNode = new Node(element);
        if (index == 0) {
            unshift(element);
            return;
        }
        if (index == size - 1) {
            last.next = newNode;
            last = newNode;
            size++;
            return;
        }
        Node oldNode = first;
        for (int i = 0; i < index; i++) {
            oldNode = oldNode.next;
        }
        Node oldPrevious = oldNode.previous;
        oldPrevious.next = newNode;
        oldNode.previous = newNode;

        newNode.previous = oldPrevious;
        newNode.next = oldNode;
        size++;
    }

    @Override public T shift() {
        T tmp = getFirstElem();
            first = first.next;
            size--;
        return tmp;
    }

    @Override public T pop() {
        T tmp = getLastElem();
            last = last.previous;
            size--;
        return tmp;
    }

    @Override public T remove(int index) throws MyException {
        Validator.checkIndex(index, size);
        if (isEmpty()) {
            return null;
        }
        Node<T> first_node = first;
        Node<T> second_node = first;
        if(index == 0 && size > 1){
            first = first.next;
            return first.element;
        }
        if(index == 0 && size == 1){
            first = null;
            return null;
        }
        for (int i = 0; i < index; i++){
            first = second_node;
            second_node = second_node.next;
        }
        first = second_node;
        size--;
        return second_node.element;
    }




    @Override public T getFirstElem() {
        if (isEmpty()) {
            return null;
        }
        return first.element;
    }

    @Override public T getLastElem() {
        if (isEmpty()) {
            return null;
        }
        return last.element;
    }

    @Override public T get(int index) throws MyException {
        Validator.checkIndex(index, size);
        Node<T> result = first;
        for (int i = 0; i < index; i++) {
            result = result.next;
        }
        return result.element;
    }

    @Override public int getSize() {
        return size;
    }


    @Override public String toString() {
            String result = "";
            if (isEmpty()) {
                return result;
            }
            Node<T> tmp = first;
            for (int i = 0; i < size; i++){
                if(tmp == null) {
                    break;
                }
                result+= i + 1 + "element = " ;
                result+= tmp.element.toString() + "\n";
                tmp = tmp.next;

            }
            return result;
        }


}
