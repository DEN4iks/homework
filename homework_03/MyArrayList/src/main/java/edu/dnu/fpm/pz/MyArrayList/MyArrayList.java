package edu.dnu.fpm.pz.MyArrayList;

import edu.dnu.fpm.pz.list.interfaces.List;
import edu.dnu.fpm.pz.myexception.MyException;
import edu.dnu.fpm.pz.validator.Validator;

public class MyArrayList<T> implements List<T> {

    private final int INIT_SIZE = 16;
    private final int CUT_RATE = 4;
    private int _size;
    private Object[] array;
    private int pointer = 0;


    public MyArrayList(int size) {
        if (size == 0) {
            makeEmptyList();
        }
        else {
            _size = size;
            array = new Object[size];
        }
    }

    public MyArrayList() {
        makeEmptyList();
    }


    private void makeEmptyList() {
        _size = 0;
        array = new Object[INIT_SIZE];
    }

    @Override public void showAll() {
        for (int i = 0; i < pointer; i++) {
            System.out.println(array[i]);
        }
    }

    @Override public T get(int index) throws MyException {
        Validator.checkIndex(index, pointer);
        return (T) array[index];
    }

    @Override public int getSize() {
        return pointer;
    }

    @Override public boolean isEmpty() {
        return getSize() == 0 ? true : false;
    }

    @Override public void add(int index, T elem) throws MyException {
        Validator.checkIndex(index, pointer);
        pointer++;
        if (pointer == array.length - 1) {
            resize(array.length * 2);
        }

        for (int i = pointer; i > index; i--) {
            array[i] = array[i - 1];
        }
        array[index] = elem;
    }

    @Override public T remove(int index) throws MyException {
        Validator.checkIndex(index, pointer);
        for (int i = index; i < pointer; i++) {
            array[i] = array[i + 1];
        }
        array[pointer] = null;
        pointer--;
        if (array.length > INIT_SIZE && pointer < array.length / CUT_RATE) {
            resize(array.length / 2);
        }

        return (T) array[pointer];
    }

    @Override public String toString() {
        String str = "";
        int i = 0;
        for (Object el : array) {
            if (el != null) {
                str = "arr[" + i + "] = " + el.toString() + "\n";
            }
        }
        return str;
    }

    @Override public void unshift(T elem) {
        if (isEmpty()) {
            array[pointer++] = elem;
        }
        else {
            if (pointer == array.length - 1) {
                resize(array.length * 2);
            }
            pointer++;
            for (int i = pointer; i > 0; i--) {
                array[i] = array[i - 1];
            }
            array[0] = elem;
        }
    }

    @Override public void push(T elem) {
        if (pointer == array.length - 1) {
            resize(array.length * 2);
        }
        array[pointer++] = elem;
    }

    @Override public T shift() throws MyException {
        return remove(0);
    }

    @Override public T pop() throws MyException {
        return remove(pointer - 1);
    }

    @Override public T getFirstElem() {
        return (T) array[0];
    }

    @Override public T getLastElem() {
        return (T) array[getSize() - 1];
    }

    private void resize(int newLength) {
        Object[] newArray = new Object[newLength];
        System.arraycopy(array, 0, newArray, 0, pointer);
        array = newArray;
    }
}
