package edu.dnu.fpm.pz.ArrayList;
import static org.junit.Assert.assertEquals;

import edu.dnu.fpm.pz.item.Item;
import edu.dnu.fpm.pz.myexception.MyException;
import org.junit.*;
import edu.dnu.fpm.pz.MyArrayList.MyArrayList;
import org.junit.rules.ExpectedException;

public class TestMyArrayList {
    @Rule
    public ExpectedException thrown = ExpectedException.none();

    private MyArrayList<Item> myarraylist;
    private Item myData = new Item(5);

    @Before
    public  void PrepareData(){
        myarraylist = new MyArrayList<>();
        myarraylist.push(myData);
    }

    @After
    public  void AfterTest(){
        myarraylist = null;
    }

    @Test
    public void testDefaultConstructor(){
        //GIVEN
        int expectedsize = 0;
        //WHEN
        myarraylist = new MyArrayList<>();
        int result = myarraylist.getSize();
        //THEN
        Assert.assertNotNull(myarraylist);
        Assert.assertEquals(expectedsize, result);
    }



    @Test
    public void testAddNegativeWithExpectedException() throws MyException {
        //GIVEN
        Item newData = new Item(40);
        int negativeIndex = -4;
        thrown.expect(MyException.class);
        thrown.expectMessage("index < 0");
        //WHEN
        myarraylist.add(negativeIndex, newData);
        thrown = ExpectedException.none();
        //THEN
    }



    @Test
    public void testAddBigIndexWithExpectedException() throws MyException {
        //GIVEN
        Item newData = new Item(40);
        int bigIndex = 100;
        thrown.expect(MyException.class);
        thrown.expectMessage("index >= size");
        //WHEN
        myarraylist.add(bigIndex, newData);
        thrown = ExpectedException.none();
        //THEN
    }

    @Test
    public void testAddGoodIndex() throws MyException
    {
        //GIVEN
        Item newData = new Item(34);
        int index = 0;
        //WHEN
        myarraylist.add(index, newData);
        //THEN
        Assert.assertNotNull(myarraylist);
        assertEquals(myarraylist.getSize(), 2);
        assertEquals(myarraylist.get(0), newData);
    }

    @Test
    public void testAddElementToBegin() throws MyException
    {
        //GIVEN
        Item myData1 = new Item(34);
        Item myData2 = new Item(5);
        Item myData3 = new Item(6);
        //WHEN
        myarraylist.unshift(myData1);
        myarraylist.unshift(myData2);
        myarraylist.unshift(myData3);
        //THEN
        Assert.assertNotNull(myarraylist);
        assertEquals(myarraylist.getSize(), 4);
        assertEquals(myarraylist.get(0), myData3);
    }

    @Test
    public void testAddElementToEnd() throws MyException
    {
        //GIVEN
        Item myData1 = new Item(34);
        Item myData2 = new Item(5);
        Item myData3 = new Item(6);
        //WHEN
        myarraylist.push(myData1);
        myarraylist.push(myData2);
        myarraylist.push(myData3);
        //THEN
        Assert.assertNotNull(myarraylist);
        assertEquals(myarraylist.getSize(), 4);
        assertEquals(myarraylist.get(myarraylist.getSize() - 1), myData3);
    }

    @Test
    public void testGetFirstElem() throws  MyException
    {
        //GIVEN
        Item newData;
        //WHEN
        newData = myarraylist.get(0);
        //THEN
        assertEquals(newData, myarraylist.getFirstElem());
    }

    @Test
    public void testGetLastElem() throws MyException
    {
        //GIVEN
        Item newData;
        //WHEN
        newData = myarraylist.get(myarraylist.getSize() - 1);
        //THEN
        assertEquals(newData, myarraylist.getLastElem());
    }


    @Test
    public void testGetNegativeIndexWithExpectedException() throws MyException {
        //GIVEN
        int negativeIndex = -4;
        thrown.expect(MyException.class);
        thrown.expectMessage("index < 0");
        //WHEN
        myarraylist.get(negativeIndex);
        thrown = ExpectedException.none();
        //THEN
    }

    @Test
    public void testGetBigIndexWithExpectedException() throws MyException {
        //GIVEN
        int bigIndex = 100;
        thrown.expect(MyException.class);
        thrown.expectMessage("index >= size");
        //WHEN
        myarraylist.get(bigIndex);
        thrown = ExpectedException.none();
        //THEN
    }

    @Test
    public void testGetElem() throws MyException
    {
        //GIVEN
        int index = 0;
        Item newData;
        //WHEN
        newData = myarraylist.get(index);
        //THEN
        Assert.assertNotNull(myarraylist);
        assertEquals(myarraylist.getSize(), 1);
        assertEquals(newData, myData);
    }


    @Test
    public void testRemoveFromBegin() throws MyException
    {
        //GIVEN
        Item myData1 = new Item(4);
        //WHEN
        myarraylist.unshift(myData1);
        myarraylist.shift();
        //THEN
        Assert.assertNotNull(myarraylist);
        assertEquals(myarraylist.getSize(), 1);
        assertEquals(myData, myarraylist.getFirstElem());
    }

    @Test
    public void testRemoveFromEnd() throws MyException
    {
        //GIVEN
        Item myData1 = new Item(4);
        //WHEN
        myarraylist.push(myData1);
        myarraylist.pop();
        //THEN
        Assert.assertNotNull(myarraylist);
        assertEquals(myarraylist.getSize(), 1);
        assertEquals(myData, myarraylist.getLastElem());
    }


    @Test
    public void testRemoveNegativeIndexWithExpectedException() throws MyException {
        //GIVEN
        int negativeIndex = -4;
        thrown.expect(MyException.class);
        thrown.expectMessage("index < 0");
        //WHEN
        myarraylist.remove(negativeIndex);
        thrown = ExpectedException.none();
        //THEN
    }


    @Test
    public void testRemoveBigIndexWithExpectedException() throws MyException {
        //GIVEN
        int bigIndex = 100;
        thrown.expect(MyException.class);
        thrown.expectMessage("index >= size");
        //WHEN
        myarraylist.remove(bigIndex);
        thrown = ExpectedException.none();
        //THEN
    }

    @Test
    public void testRemoveElem() throws MyException
    {
        //GIVEN
        int index = 1;
        Item myData1 = new Item(3);
        Item myData2 = new Item(5);
        //WHEN
        myarraylist.push(myData1);
        myarraylist.push(myData2);
        myarraylist.remove(index);
        //THEN
        Assert.assertNotNull(myarraylist);
        assertEquals(myarraylist.getSize(), 2);
        assertEquals(myarraylist.get(index), myData2);
    }
}

