package edu.dnu.fpm.pz.myexception;

public class MyException extends Exception {
    private String message;

    public MyException(String _message)
    {
        super(_message);
        message = _message;
    }
    public void toLog()
    {
        System.out.println(message);
    }
}


