package edu.dnu.fpm.pz.list.console;
import edu.dnu.fpm.pz.MyArrayList.MyArrayList;
import edu.dnu.fpm.pz.MyLinkedList.MyLinkedList;
import edu.dnu.fpm.pz.item.Item;
import edu.dnu.fpm.pz.list.interfaces.List;
import edu.dnu.fpm.pz.myexception.MyException;

import java.util.NoSuchElementException;



public class ListConsole {
    public static void listDemo(List<Item> list) throws MyException
    {
        Item myData = new Item(78);

        System.out.println("Array list work demonstration:");

        System.out.println("\nPushing myData: " + myData + " => ");
        list.push(myData);
        list.showAll();

        System.out.println("\nPushing number(123):  => ");
        list.push(new Item(123));
        list.showAll();

        System.out.println("\nPushing number(83):  => ");
        list.push(new Item(83));
        list.showAll();

        System.out.println("\nPop last element:  => ");
        list.pop();
        list.showAll();

        System.out.println("\nUnshift number(999):  => ");
        list.unshift(new Item(999));
        list.showAll();

        System.out.println("\nShift first element:  => ");
        list.shift();
        list.showAll();
    }

    public static void main( String[] args )
    {
        try {
            listDemo(new MyLinkedList<Item>());
//            listDemo(new MyArrayList<Item>());
        }
        catch (MyException ex)
        {
            ex.toLog();
        }
    }
}
